{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib notebook\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import firedrake\n",
    "from firedrake import inner, grad, dx, ds, dS, assemble"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we'll demonstrate a few things around the variational form of the Poisson equation.\n",
    "We'll use a square domain with a source in the upper left corner."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nx, ny = 64, 64\n",
    "mesh = firedrake.UnitSquareMesh(nx, ny)\n",
    "Q = firedrake.FunctionSpace(mesh, family='CG', degree=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y = firedrake.SpatialCoordinate(mesh)\n",
    "r = firedrake.sqrt(((x - 0.25)**2 + (y - 0.75)**2))\n",
    "R = 1/6\n",
    "f = firedrake.max_value(0, 1 - (r / R)**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(firedrake.interpolate(f, Q), cmap='magma', plot3d=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We'll use a conductivity coefficient that changes from 1 to 10 across an interface that cuts diagonally through the domain.\n",
    "When the coefficients are discontinuous, it becomes much more difficult to apply the finite difference method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = firedrake.conditional((y - 0.5) - 4 * (x - 0.5) > 0, 10, 1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "P = firedrake.FunctionSpace(mesh, family='DG', degree=0)\n",
    "firedrake.plot(firedrake.interpolate(k, P), cmap='magma', plot3d=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now create the action for our problem:\n",
    "\n",
    "$$J(\\phi) = \\int_\\Omega\\left(\\frac{1}{2}k|\\nabla\\phi|^2 - f\\phi\\right)dx + \\frac{1}{2}\\int_{\\partial\\Omega}\\sigma(\\phi - g)^2ds.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ϕ = firedrake.Function(Q)\n",
    "r = firedrake.Constant(1.)\n",
    "σ = k / r\n",
    "J = (0.5 * k * inner(grad(ϕ), grad(ϕ)) - f * ϕ) * dx + 0.5 * σ * ϕ**2 * ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see what the objective functional is equal to when we start out:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(assemble(J))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember all that work we did in order to derive what the Euler-Lagrange equations are for an arbitrary functional?\n",
    "Hold my beer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "F = firedrake.derivative(J, ϕ)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code will solve the PDE using a direct method (LU factorization of the matrix)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parameters = {'solver_parameters': {'ksp_type': 'preonly', 'pc_type': 'lu'}}\n",
    "firedrake.solve(F == 0, ϕ, **parameters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(ϕ, plot3d=True, cmap='magma')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The objective functional is lower than what we started with.\n",
    "Fun exercise: perturb the solution by a random trigonometric polynomial and see how the action changes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('{:e}'.format(assemble(J)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next I'll give a little after-the-fact demonstration of the conservation law.\n",
    "First, we'll look at a control volume in the lower right part of the domain, where there's no heating."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "χ_K = firedrake.conditional(firedrake.And(0.625 < x,\n",
    "                             firedrake.And(x < 0.875,\n",
    "                              firedrake.And(0.125 < y, y < 0.375))), 1, 0)\n",
    "ψ = firedrake.project(χ_K, P)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.plot(ψ, plot3d=True, cmap='magma')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The flux through this control volume is roughly 0, which makes sense because there are no heat sources inside it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ν = firedrake.FacetNormal(mesh)\n",
    "q = -k * inner(grad(ϕ), ν)\n",
    "form = 0.5 * (q('+') - q('-')) * (ψ('+') - ψ('-')) * dS\n",
    "print(assemble(form))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But if we consider a control volume that intersects the region where there's heat being applied, the flux is non-zero."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "χ_K = firedrake.conditional(firedrake.And(0.125 < x,\n",
    "                             firedrake.And(x < 0.375,\n",
    "                              firedrake.And(0.625 < y, y < 0.875))), 1, 0)\n",
    "ψ = firedrake.project(χ_K, P)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ν = firedrake.FacetNormal(mesh)\n",
    "q = -k * inner(grad(ϕ), ν)\n",
    "flux = 0.5 * (q('+') - q('-')) * (ψ('+') - ψ('-')) * dS\n",
    "source = f * ψ * dx\n",
    "print('heat flux through the box:                +{:0.04e}'.format(assemble(flux)))\n",
    "print('heat flux through the box - heat sources: {:0.04e}'.format(assemble(flux - source)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Just for kicks, let's write out the weak form of the problem explicitly rather than as the derivative of the action."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Φ = firedrake.TrialFunction(Q)\n",
    "Ψ = firedrake.TestFunction(Q)\n",
    "\n",
    "a = k * inner(grad(Φ), grad(Ψ)) * dx + σ * Φ * Ψ * ds\n",
    "L = f * Ψ * dx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The solution of the weak form of the problem is the same up to rounding error as that of the minimization form."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ψ = firedrake.Function(Q)\n",
    "firedrake.solve(a == L, ψ, **parameters)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "firedrake.norm(ϕ - ψ) / firedrake.norm(ϕ)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "firedrake",
   "language": "python",
   "name": "firedrake"
  },
  "language_info": {
   "name": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
