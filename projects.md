# Projects

Fun things we could do if you like this sort of thing:

* a buoyant plume model for sub-ice shelf melting
* sediment transport, glacial landscape evolution, tidewater glacier cycle
* gradient flow formulation of hillslope transport
* velocity-pressure-stress formulation of Stokes equations
* assimilating radio echo sounding data to make bed maps
* variational methods for grid adaption
* calving front evolution through phase field method
* reformulating Mauro Werder's hydrology model
